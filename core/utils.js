var clc = require('cli-color');

// var colors = require('colors');
// colors.setTheme({
//     silly   : 'rainbow',
//     input   : 'grey',
//     verbose : 'cyan',
//     prompt  : 'grey',
//     info    : 'green',
//     data    : 'grey',
//     help    : 'cyan',
//     warn    : 'yellow',
//     debug   : 'blue',
//     error   : 'red'
// });




var rand = function(max, min) {
    min = min === undefined ? 0 : min;
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

var randItem = function(arr) {
    return arr[rand(arr.length - 1)];
};

var logline = function(text, color) {
    process.stdout.write(clc[color || "white"](text));
};

var log = function(text, color) {
    logline("\n" + text, color);
};


module.exports = {
    rand        : rand,
    randItem    : randItem,
    logline     : logline,
    log         : log
};
