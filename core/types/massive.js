var async   = require("async");
var request = require("request");
var Utils   = require("../utils");


var Massive = function(options, requests) {
    this.options = options || {};
    this.requests = requests || [];
    this.timeCounter = 0;
    return this;
};


Massive.prototype = {

    _createRequestTask: function(data) {
        var that = this;
        return function(cb) {
            var started = new Date();
            Utils.logline("·", "green");
            request(data, function(err, res) {
                that.timeCounter += new Date() - started;
                if(!res) console.error(err);
                else {
                    if(res.statusCode !== 200) {
                        console.log(res);
                        console.log("==============================");
                    }
                }
                cb(err, "Done");
            });
        }
    },

    addRequest: function(req_data) {
        this.requests.push(req_data);
        return this;
    },

    getResultsByTime: function(time, callback) {
        var _start_time = new Date();
        var _counter = 0;
        async.whilst(
            function() {
                return new Date() - _start_time < time;
            },
            this._createRequestTask(Utils.randItem(this.requests)),
            function() {
                var average = (new Date() - _start_time) / 1000 / _counter;
                callback.call(callback, _counter, average);
            }
        );
        return this;
    },

    benchmark: function(num_requests, concurrency, callback) {
        var _start_time = new Date();
        var tasks = [];
        this.timeCounter = 0;
        var that = this;

        for(var i = 0; i < num_requests; i++) {
            tasks.push(this._createRequestTask(Utils.randItem(this.requests)));
        }
        async.parallelLimit(tasks, concurrency, function(err, res) {
            var time = new Date() - _start_time;
            var avg = (num_requests / time * 100).toFixed(2);
            Utils.log("[BENCHMARK RESULTS]", "red");
            Utils.log(" Num Requests            :: " + num_requests, "green");
            Utils.log(" Concurrency             :: " + concurrency, "green");
            Utils.log(" Total time elapsed      :: " + (time / 1000).toFixed(3) + "s", "green");
            Utils.log(" Average                 :: " + (that.timeCounter / num_requests / 1000).toFixed(3) + "s", "green");
            Utils.log(" Num requests per second :: " + (num_requests / time * 1000).toFixed(3), "green");
            Utils.log("");
            callback.call(callback, time);
        });
        return this;
    }
};


module.exports = Massive;










