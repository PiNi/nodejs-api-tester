var async   = require("async");
var request = require("request");


var _createRequestTask = function(data) {
    return function(cb) {
        request(data, function(err, res) {
            cb(err, res);
        });
    }
};

var Response = function(global_request_options) {
    this.req = global_request_options || {};
    return this;
};

Response.prototype = {

    _updateUrlQS: function() {
        var qparams = [];
        for(var key in this.req.form) qparams.push(key + "=" + this.req.form[key]);
        this.req.url = this.req.url + "?" + qparams.join("&");
    },

    setData: function(data) {
        for(var key in data) this.req[key] = data[key];
        return this;
    },

    test: function(expected_status, callback) {
        if(this.req.method.toLowerCase() === "get") this._updateUrlQS();
        request(this.req, function(err, res) {
            if(err) console.error("[ERROR] :: ", err.code);
            if(res.statusCode !== expected_status) {
                console.error("Incorrect status code (" + expected_status + " expected, " + res.statusCode + " returned)");
            }
            callback.call(callback, err, res);
        });
    }
};



module.exports = Response;
