(function() {

    var _runPromisedFunctions = function(functions, secuentialy, shielded) {
        var total_count = functions.length;
        var promise = new Promise();
        var errors = [];
        var results = [];
        var done = 0;

        function check_for_end(index) {
            return function(error, result) {
                errors[index] = error;
                results[index] = result;
                done++;
                if(done === total_count || (shielded === true && !!error)) {
                    promise.done(errors, results);
                } else if(secuentialy) {
                    functions[done](error, result).then(check_for_end(done));
                }
            };
        };

        if(secuentialy === true) {
            functions[0]().then(check_for_end(0));
        } else {
            functions.forEach(function(cb, index) {
                cb().then(check_for_end(index));
            });
        }

        return promise;
    };


    var Promise = function() {
        this._callbacks = [];
        this._finished = false;
        return this;
    };

    Promise.prototype = {

        then: function(callback, context) {
            var _callback = function() {
                return callback.apply(context, arguments);
            };
            if(this._finished) _callback(this.error, this.result);
            else this._callbacks.push(_callback);
        },

        done: function(error, result) {
            this._finished = true;
            this.error = error;
            this.result = result;
            this._callbacks.forEach(function(cb) {
                cb(error, result);
            });
            this._callbacks = [];
        }
    };


    Promise.parallel = function(functions) {
        return _runPromisedFunctions(functions, false, false);
    };
    Promise.chain = function(functions) {
        return _runPromisedFunctions(functions, true, false);
    };
    Promise.shield = function(functions) {
        return _runPromisedFunctions(functions, true, true);
    };


    if (typeof module !== 'undefined' && module.exports) {
        module.exports = Promise;
    } else {
        window.Promise = Promise;
    };

})();